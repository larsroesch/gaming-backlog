package szut.android.gamingbacklog.data.models

import androidx.room.Entity

@Entity(primaryKeys = ["backlogId", "tagId"], tableName = "entry_tags")
data class BacklogEntryTagCrossRef(
    var backlogId: Int,
    var tagId: Int
) {
    constructor() : this(0, 0)
}
