package szut.android.gamingbacklog.data.dataaccess

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import szut.android.gamingbacklog.data.models.BacklogEntry
import szut.android.gamingbacklog.data.models.BacklogEntryTagCrossRef
import szut.android.gamingbacklog.data.models.Tag

@Database(
    entities = [
        BacklogEntry::class,
        Tag::class,
        BacklogEntryTagCrossRef::class
    ],
    version = 1
)
@TypeConverters(Converters::class)
abstract class BacklogDatabase : RoomDatabase() {

    abstract fun backlogDao(): BacklogDao

    abstract fun tagDao(): TagDao

    companion object {
        @Volatile
        private var INSTANCE: BacklogDatabase? = null;

        fun getDatabase(context: Context): BacklogDatabase {
            val tempInstance = INSTANCE;
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BacklogDatabase::class.java,
                    "gaming_backlog_db"
                ).build()
                INSTANCE = instance
                return instance
            }
        }

    }

}