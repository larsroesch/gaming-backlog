package szut.android.gamingbacklog.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "backlog_entries")
data class BacklogEntry(
    @PrimaryKey(autoGenerate = true)
    val backlogId: Int,
    var title: String,
    var plattform: String,
    var status: Status = Status.BACKLOG,
    var priority: Int = 0,
    var release: Date,
    var playTime: String,
    var notice: String
) {

    constructor() : this(0, "", "", Status.BACKLOG, 0, Date(), "", "")

}