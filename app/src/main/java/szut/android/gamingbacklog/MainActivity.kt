package szut.android.gamingbacklog

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import szut.android.gamingbacklog.data.models.BacklogEntry


class MainActivity : AppCompatActivity(R.layout.activity_main) {
    val testList: ArrayList<BacklogEntry> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpGeneralStuff()
        val fabButton = findViewById<FloatingActionButton>(R.id.floating_button)
        fabButton.setOnClickListener {
            val intent = Intent(this, FormularActivity::class.java)
            startActivity(intent)
        }
    }


    fun setUpGeneralStuff(){
        findViewById<androidx.appcompat.widget.Toolbar>(R.id.main_toolbar).setTitle(R.string.app_name)
        findViewById<FloatingActionButton>(R.id.floating_button).setOnClickListener {
            val intent = Intent(this, FormularActivity::class.java)
            startActivity(intent)

        }
        val viewPager = findViewById<ViewPager>(R.id.main_activity_viewpager)
        val adapterViewPager = MyPagerAdapter(supportFragmentManager);
        viewPager.adapter = adapterViewPager
        val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        tabLayout.setupWithViewPager(viewPager)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onTestEventgreen(event: MessageEvent) {
        finish()
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }

}

class MyPagerAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> BacklogFragment()
            1 -> PlayingFragment.newInstance()
            2 -> FinishedFragment.newInstance()
            else -> Fragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Backlog"
            1 -> "Am Spielen"
            2 -> "Beendet"
            else -> "FEHLER D:"
        }
    }

    companion object {
        private const val NUM_ITEMS = 3
    }
}

class MessageEvent