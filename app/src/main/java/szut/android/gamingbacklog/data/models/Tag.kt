package szut.android.gamingbacklog.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "backlog_tags")
data class Tag(
    @PrimaryKey
    val tagId: Int,
    var name: String
) {
    constructor() : this(0, "")

    override fun toString(): String = name
}