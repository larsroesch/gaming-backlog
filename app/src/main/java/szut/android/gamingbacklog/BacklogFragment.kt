package szut.android.gamingbacklog

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import szut.android.gamingbacklog.data.dataaccess.BacklogDatabase
import szut.android.gamingbacklog.data.models.EntryWithTag
import szut.android.gamingbacklog.data.models.Status
import szut.android.gamingbacklog.gears.RVAdapter


class BacklogFragment : Fragment() {
    var recyclerView: RecyclerView? = null
    var testList: MutableList<EntryWithTag>? = null
    var recyclerViewAdapter: RVAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(R.layout.fragment_backlog, container, false)
        container?.let { loadBacklogEntry(it.context) }
        recyclerView = view.findViewById<View>(R.id.backlog_recyclerview) as RecyclerView
        recyclerViewAdapter = RVAdapter(activity, testList)
        val layoutManager = LinearLayoutManager(activity)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.adapter = recyclerViewAdapter
        return view
    }

    fun loadBacklogEntry(context: Context) {
        val database = BacklogDatabase.getDatabase(context)
        testList = ArrayList()
        GlobalScope.launch {
            testList?.addAll(database.backlogDao().getEntryWithTagsByStatus(Status.BACKLOG))
        }
    }

}