package szut.android.gamingbacklog

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.textChangedListener
import szut.android.gamingbacklog.data.dataaccess.BacklogDao
import szut.android.gamingbacklog.data.dataaccess.BacklogDatabase
import szut.android.gamingbacklog.data.models.BacklogEntry
import szut.android.gamingbacklog.data.models.Status

class FormularActivity : AppCompatActivity(R.layout.activity_formular) {

    val backlogEntry: BacklogEntry = BacklogEntry()
    var dao: BacklogDao = BacklogDatabase.getDatabase(this).backlogDao()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initSpinner(
            findViewById<Spinner>(R.id.formular_status),
            R.array.status_array,
            true,
            backlogEntry
        )
        initSpinner(
            findViewById<Spinner>(R.id.formular_prio),
            R.array.prio_array,
            false,
            backlogEntry
        )
        findViewById<EditText>(R.id.formular_title)?.textChangedListener {
            afterTextChanged {
                backlogEntry.title = it.toString()
            }
        }
        findViewById<EditText>(R.id.formular_plattform)?.textChangedListener {
            afterTextChanged {
                backlogEntry.plattform = it.toString()
            }
        }
        findViewById<EditText>(R.id.formular_playtime)?.textChangedListener {
            afterTextChanged {
                backlogEntry.playTime = it.toString()
            }
        }
        findViewById<Button>(R.id.formular_button)?.setOnClickListener {
            GlobalScope.launch { dao.insertBacklogEntry(backlogEntry) }
            this.finish()
            EventBus.getDefault().post(MessageEvent())
        }
    }

    fun initSpinner(spinner: Spinner, arrayRes: Int, isStatus: Boolean, backlogEntry: BacklogEntry) {
        val adapter = ArrayAdapter(
            this,
            R.layout.custom_spinner_item, resources.getStringArray(arrayRes)
        )
        spinner.adapter = adapter
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                if (isStatus){
                    backlogEntry.status = when (spinner.selectedItem.toString()) {
                        "BACKLOG" -> Status.BACKLOG
                        "IN_PROGRESS" -> Status.IN_PROGRESS
                        "FINISHED" -> Status.FINISHED
                        else -> Status.BACKLOG
                    }
                }
                if (!isStatus){
                    backlogEntry.priority = when (spinner.selectedItem.toString()) {
                        "Gering" -> 0
                        "Normal" -> 1
                        "Hoch" -> 2
                        "eXtReM wIcHtIg!!11!!!" -> 3
                        else -> 4
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

    }



}