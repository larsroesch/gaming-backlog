package szut.android.gamingbacklog.gears

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import szut.android.gamingbacklog.R
import szut.android.gamingbacklog.data.models.EntryWithTag


class RVAdapter(mcContext: Context?, liste: List<EntryWithTag>?) :
    RecyclerView.Adapter<ItemViewHolder>() {
    var mcContext: Context? = null
    var liste: List<EntryWithTag>? = null

    init {
        this.mcContext = mcContext
        this.liste = liste
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_game, parent, false)
        return ItemViewHolder(view, mcContext)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val backlogEntry: EntryWithTag = liste!![position]
        holder.bind(backlogEntry)
    }

    override fun getItemCount(): Int {
        return liste!!.size
    }
}