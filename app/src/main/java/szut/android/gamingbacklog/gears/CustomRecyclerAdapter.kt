package szut.android.gamingbacklog.gears

import androidx.recyclerview.widget.RecyclerView
import szut.android.gamingbacklog.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.list_item_game.view.*
import szut.android.gamingbacklog.data.models.BacklogEntry

class CustomRecyclerAdapter(val items : ArrayList<BacklogEntry>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_game, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder?.tvAnimalType?.text = items.get(position)
        holder.title.text = items.get(position).title
        holder.platform.text = items.get(position).plattform
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each animal to
    val title = view.list_item_title
    val platform = view.list_item_plattform

}