package szut.android.gamingbacklog.data.dataaccess

import androidx.room.TypeConverter
import szut.android.gamingbacklog.data.models.Status
import java.util.*

class Converters {

    @TypeConverter
    fun toStatus(value: Int) = enumValues<Status>()[value];

    @TypeConverter
    fun fromStatus(value: Status) = value.ordinal

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }

}