package szut.android.gamingbacklog.data.models

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class EntryWithTag(
    @Embedded
    var entry: BacklogEntry,
    @Relation(
        parentColumn = "backlogId",
        entityColumn = "tagId",
        associateBy = Junction(BacklogEntryTagCrossRef::class)
    )
    var tags: List<Tag>
) {
    constructor() : this(BacklogEntry(), listOf<Tag>())
}
