package szut.android.gamingbacklog.gears


import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import szut.android.gamingbacklog.R
import szut.android.gamingbacklog.data.models.EntryWithTag


class ItemViewHolder(itemView: View, context: Context?) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener {

    var layout: ConstraintLayout? = null
    var context: Context? = null

    var id: TextView? = null
    var title: TextView? = null
    var platform: TextView? = null
    var status: TextView? = null
    var priority: TextView? = null
    var release: TextView? = null
    var playTime: TextView? = null
    var notice: TextView? = null
    var tags: TextView? = null

    init {
        this.context = context
        itemView.isClickable = true
        layout = itemView.findViewById(R.id.list_item_game_element)
        title = itemView.findViewById(R.id.list_item_title)
        platform = itemView.findViewById(R.id.list_item_plattform)
        priority = itemView.findViewById(R.id.list_item_priority)
        playTime = itemView.findViewById(R.id.list_item_playtime)
        tags = itemView.findViewById(R.id.list_item_tags)
    }

    fun bind(backlogEntryWithTags: EntryWithTag) {
        var backlogEntry = backlogEntryWithTags.entry

        itemView.setOnClickListener(this)

        title?.text = backlogEntry.title
        platform?.text = "Platform:  ${backlogEntry.plattform}"
        priority?.text = when (backlogEntry.priority) {
            0 -> "Prio: NIEDRIG"
            1 -> "Prio: NORMAL"
            2 -> "Prio: HOCH"
            else -> "Prio: SEHR HOCH"
        }
        playTime?.text = "Spielzeit: ${backlogEntry.playTime}h"
        tags?.text = "Tags:\n" + backlogEntryWithTags.tags.joinToString(", ")
    }

    override fun onClick(p0: View?) {
        print("HALLO")
    }

}