package szut.android.gamingbacklog.data.dataaccess

import androidx.room.*
import szut.android.gamingbacklog.data.models.BacklogEntry
import szut.android.gamingbacklog.data.models.BacklogEntryTagCrossRef
import szut.android.gamingbacklog.data.models.EntryWithTag
import szut.android.gamingbacklog.data.models.Status

@Dao
interface BacklogDao {

    @Query("SELECT * FROM backlog_entries")
    suspend fun getAll(): List<BacklogEntry>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBacklogEntry(entry: BacklogEntry)

    @Update
    suspend fun updateBacklogEntry(entry: BacklogEntry)

    @Delete
    suspend fun deleteBacklogEntry(entry: BacklogEntry)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBacklogEntryTagsCrossRef(crossRef: BacklogEntryTagCrossRef)

    @Query("SELECT * FROM backlog_entries WHERE backlogId = :id")
    suspend fun getById(id: Int): BacklogEntry

    @Query("SELECT * FROM backlog_entries WHERE backlogId IN (:ids)")
    suspend fun getAllById(ids: List<Int>): List<BacklogEntry>

    @Query("SELECT * FROM backlog_entries WHERE status = :status")
    suspend fun getAllByStatus(status: Status): List<BacklogEntry>

    @Transaction
    @Query("SELECT * FROM backlog_entries")
    suspend fun getEntryWithTag(): List<EntryWithTag>

    @Transaction
    @Query("SELECT * FROM backlog_entries WHERE backlogId = :id")
    suspend fun getEntryWithTagById(id: Int): List<EntryWithTag>

    @Transaction
    @Query("SELECT * FROM backlog_entries WHERE status = :status")
    suspend fun getEntryWithTagsByStatus(status: Status): List<EntryWithTag>

}