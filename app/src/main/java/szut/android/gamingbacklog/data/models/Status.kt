package szut.android.gamingbacklog.data.models

enum class Status {

    BACKLOG,
    IN_PROGRESS,
    FINISHED

}