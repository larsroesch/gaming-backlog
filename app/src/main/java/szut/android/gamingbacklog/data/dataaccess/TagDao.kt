package szut.android.gamingbacklog.data.dataaccess

import androidx.room.*
import szut.android.gamingbacklog.data.models.BacklogEntryTagCrossRef
import szut.android.gamingbacklog.data.models.Tag
import szut.android.gamingbacklog.data.models.TagWithEntry

@Dao
interface TagDao {

    @Query("SELECT * FROM backlog_tags")
    suspend fun getAll(): List<Tag>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTag(tag: Tag)

    @Update
    suspend fun updateTag(tag: Tag)

    @Delete
    suspend fun deleteTag(tag: Tag)

    @Query("SELECT * FROM backlog_tags WHERE tagId = :id")
    suspend fun getById(id: Int): Tag

    @Query("SELECT * FROM backlog_tags WHERE tagId IN (:ids)")
    suspend fun getAllById(ids: List<Int>): List<Tag>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBacklogEntryTagsCrossRef(crossRef: BacklogEntryTagCrossRef)

    @Transaction
    @Query("SELECT * FROM backlog_tags")
    suspend fun getTagWithEntries(): List<TagWithEntry>

}