package szut.android.gamingbacklog.data.models

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class TagWithEntry(
    @Embedded
    var tag: Tag,
    @Relation(
        parentColumn = "tagId",
        entityColumn = "backlogId",
        associateBy = Junction(BacklogEntryTagCrossRef::class)
    )
    var entries: List<BacklogEntry>
) {
    constructor() : this(Tag(), listOf<BacklogEntry>())
}